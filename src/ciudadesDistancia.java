import java.util.Scanner;

public class ciudadesDistancia {
	public static void main(String[] args) {
		 String distancia [] = {
				"albacete",
				"alicante,171",
				"almeria,369,294",
				"avila,366,537,663",
				"badajoz,525,696,604,318",
				"barcelona,540,515,809,717,1022",
				"bilbao,646,817,958,401,694,620"};
		
		String distancias [] [] = new String [distancia.length] [];
		for (int i = 0; i<distancias.length;i++){
			distancias[i] = distancia [i].split(",");
		}

		do {
			int ciudad1 = menuCalculoCiudad1();
			int ciudad2 = menuCalculoCiudad2();
			calculoDistancia(ciudad1,ciudad2,distancias);
		}while (true);

	}
	/**
	 * Método que pregunta la ciudad que desea el usuario
	 * @return la ciudad 1 elegida
	 */
	public static int menuCalculoCiudad1() {
		Scanner sc = new Scanner (System.in);
		System.out.println("------------------------------------------------------------");
		System.out.println("Introduce dos ciudades y calculo la distancias entre ellas: ");
		System.out.println("0: Albacete // 1: Alicante // 2: Almeria // 3: Avila // 4: Badajoz // 5: Barcelona // 6: Bilbao");
		System.out.print("Introduce la primera ciudad: ");
		int ciudad1 = sc.nextInt();
		return ciudad1;
	}
	/**
	 * Método que pregunta la segunda ciudad que desea el usuario
	 * @return la ciudad 2 elegida
	 */
	public static int menuCalculoCiudad2() {
		Scanner sc = new Scanner (System.in);
		System.out.print("Introduce la segunda ciudad: ");
		int ciudad2 = sc.nextInt();
		return ciudad2;
	}
	/**
	 * Metodo que muestra la distancia entre las dos ciudades
	 * @param ciudad1,ciudad2 y las distancias
	 */
	public static void calculoDistancia (int ciudad1,int ciudad2, String distancias [][]) {
		if(distancias[ciudad1].length>distancias[ciudad2].length) {
			System.out.println("La distancia entre "+distancias[ciudad1][0]+" y "+ distancias[ciudad2][0] +" es: "+ distancias[ciudad1][ciudad2+1]);
		}else {
			System.out.println("La distancia entre "+distancias[ciudad1][0]+" y "+ distancias[ciudad2][0] +" es: "+ distancias[ciudad2][ciudad1+1]);
		}
	}
	
}
